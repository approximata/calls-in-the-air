export enum ApiStates {
    INIT = 'INIT',
    LOADING = 'LOADING',
    SUCCESS = 'SUCCESS',
    ERROR = 'ERROR',
}

export const PageSize = 10;

export const UrlBase = 'https://frontend-test-api.aircall.io';
