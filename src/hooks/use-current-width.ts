import { useState, useEffect } from 'react';

const getWidth = () =>
    window.innerWidth 

export const useCurrentWidth = () => {
    let [width, setWidth] = useState(getWidth());

    useEffect(() => {
        const resizeListener = () => {
            setWidth(getWidth());
        };
        window.addEventListener('resize', resizeListener);
        return () => {
            window.removeEventListener('resize', resizeListener);
        };
    }, []);

    return width;
}
