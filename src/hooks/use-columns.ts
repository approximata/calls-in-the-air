import { GridCellParams, GridColDef, GridValueGetterParams } from '@material-ui/data-grid';
import { useState, useEffect } from 'react';
import { mapCall, getDateInFormat, showDuration } from '../helpers/callHelper';


interface CallIconProps {
    callType: string;
    direction: string;
}

const initColumns: GridColDef[] = [
    {
        field: 'callVisual',
        headerName: 'type',
        filterable: true,
        width: 50,
        valueGetter: (params: GridValueGetterParams): CallIconProps => ({
            callType: `${params.getValue(params.id, 'call_type')}`,
            direction: `${params.getValue(params.id, 'direction')}`
        }),
        renderCell: (params: GridCellParams) => {
            const { callType, direction } = params.value as CallIconProps;
            return mapCall(callType, direction);
        },
    },
    {
        field: 'from',
        headerName: 'from',
        sortable: true,
        filterable: true,
        flex: 1,
    },
    {
        field: 'to',
        headerName: 'to',
        sortable: true,
        filterable: true,
        flex: 1,
    },
    {
        field: 'created_at',
        headerName: 'created',
        sortable: true,
        filterable: true,
        flex: 1,
        valueGetter: (params: GridValueGetterParams): string =>
            getDateInFormat(params.value?.toString() || ''),
    },
    {
        field: 'duration',
        headerName: 'duration',
        filterable: true,
        flex: 1,
        valueGetter: (params: GridValueGetterParams): string =>
            showDuration(Number(params.value)),
    },
    {
        field: 'id',
        hide: true,
    },
];


export const useColumns = (width: number) => {
    const [columns, setColumns] = useState<GridColDef[]>(initColumns);

    useEffect(() => {

        const handleColumns = (width: number) => {
            if (width < 500) {
                const newColumns = initColumns.filter(
                    (column) =>
                        column.field === 'callVisual' ||
                        column.field === 'from' ||
                        column.field === 'to' ||
                        column.field === 'id'
                );
                setColumns([...newColumns]);
            } else {
                setColumns(initColumns);
            }
        };
        handleColumns(width);
    }, [width]);

    return columns

} 

