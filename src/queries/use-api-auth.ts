import { useState, useEffect, useRef } from 'react';
import { ApiStates, UrlBase } from '../constant';
import { getData } from '../helpers/apiHelper';
import { AuthResponse, ApiProps, UserInput } from '../interfaces';

const useApiAuth = ({
    username,
    password,
}: UserInput): AuthResponse & ApiProps => {
    const [response, setResponse] = useState<AuthResponse & ApiProps>({
        apiState: ApiStates.INIT,
        apiError: '',
        access_token: '',
        user: { id: '', username: '' },
    });

    const fetchData = useRef(({ username, password }: UserInput) => {});

    fetchData.current = async ({ username, password }: UserInput) => {
        if (username.length === 0 || password.length === 0) return;

        const setPartialResponse = (
            partialResponse: Partial<Response & ApiProps>
        ) => setResponse({ ...response, ...partialResponse });

        setPartialResponse({
            apiState: ApiStates.LOADING,
        });

        const url = `${UrlBase}/auth/login`;

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        try {
            const data = await getData({
                url,
                request: {
                    headers,
                    method: 'POST',
                    body: JSON.stringify({ username, password }),
                },
            });
            setPartialResponse({
                apiState: ApiStates.SUCCESS,
                ...data,
            });
        } catch (error) {
            setPartialResponse({
                apiState: ApiStates.ERROR,
                apiError: `Fetch failed: ${error}`,
            });
        }
    };

    useEffect(() => {
        fetchData.current({ username, password });
    }, [username, password]);

    return response;
};

export default useApiAuth;
