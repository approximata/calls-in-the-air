import { useState, useEffect, useRef } from 'react';
import { ApiStates, UrlBase } from '../constant';
import { getData } from '../helpers/apiHelper';
import { Response, ApiProps, AuthResponse, LoginProps } from '../interfaces';

interface Refresh {
    hasToRefresh: boolean;
}

const useApiRefresh = ({
    hasToRefresh,
    access,
    setAccess,
}: LoginProps & Refresh): Partial<AuthResponse & ApiProps> => {
    const [response, setResponse] = useState<
        Partial<AuthResponse> & Partial<ApiProps>
    >({
        apiState: ApiStates.INIT,
    });

    const fetchData = useRef((url: string) => {});

    fetchData.current = async (url) => {
        if (!access.user.id && !hasToRefresh) {
            console.log(access.user.id);
            console.log(hasToRefresh);
            return;
        }

        const setPartialResponse = (
            partialResponse: Partial<Response & ApiProps>
        ) => setResponse({ ...response, ...partialResponse });

        setPartialResponse({
            apiState: ApiStates.LOADING,
        });

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', `Bearer ${access.access_token}`);

        try {
            const data = await getData({
                url,
                request: { headers, method: 'POST' },
            });
            setPartialResponse({
                apiState: ApiStates.SUCCESS,
            });
            setAccess({
                access_token: data.access_token!,
                user: {
                    id: data.user!.id!,
                    username: data.user!.username!,
                },
            });
        } catch (error) {
            setPartialResponse({
                apiState: ApiStates.ERROR,
                apiError: `Fetch failed: ${error}`,
            });
        }

    
    };

    useEffect(() => {
        const url = `${UrlBase}/auth/refresh-token`;
        fetchData.current(url);
    }, [access.user.id, hasToRefresh]);

    return response;
};

export default useApiRefresh;
