import { useState, useEffect, useRef } from 'react';
import { ApiStates, PageSize, UrlBase } from '../constant';
import { getData } from '../helpers/apiHelper';
import { Response, ApiProps } from '../interfaces';

const useApiCalls = (
    page: number,
    access_token: string
): Response & ApiProps => {
    const [response, setResponse] = useState<Response & ApiProps>({
        apiState: ApiStates.INIT,
        apiError: '',
        nodes: [],
        totalCount: 0,
        hasNextPage: false,
    });

    const fetchData = useRef((url: string) => {});

    fetchData.current = async (url) => {
        if (!access_token) return;

        const setPartialResponse = (
            partialResponse: Partial<Response & ApiProps>
        ) => setResponse({ ...response, ...partialResponse });

        setPartialResponse({
            apiState: ApiStates.LOADING,
        });

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', `Bearer ${access_token}`);

        try {
            const data = await getData({ url, request: { headers } });
            setPartialResponse({
                apiState: ApiStates.SUCCESS,
                ...data,
            });
        } catch (error) {
            setPartialResponse({
                apiState: ApiStates.ERROR,
                apiError: `Fetch failed: ${error}`,
            });
        }
    };

    useEffect(() => {
        const offset = page * PageSize;
        const url = `${UrlBase}/calls?offset=${offset}&limit=${PageSize}`;
        fetchData.current(url);
    }, [page, access_token, fetchData]);

    return response;
};

export default useApiCalls;
