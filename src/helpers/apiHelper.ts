export interface GetData {
    url: string;
    request: {
        headers?: Headers;
        method?: string;
        body?: string;
    };
}

export const getData = async ({
    url,
    request: { headers, method, body },
}: GetData) => {
    let response = await fetch(url, {
        method: method || 'GET',
        headers: headers,
        body: body,
    });
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return await response.json();
};


