import { ReactElement } from "react";
import CallMissedIcon from '@material-ui/icons/CallMissed';
import CallMissedOutgoingIcon from '@material-ui/icons/CallMissedOutgoing';
import CallMadeIcon from '@material-ui/icons/CallMade';
import CallReceivedIcon from '@material-ui/icons/CallReceived';
import VoicemailIcon from '@material-ui/icons/Voicemail';
import moment from 'moment';
import 'moment-duration-format';

export const mapCall = (callType: string, direction: string): ReactElement => {
    const generalStyles = {
        margin: '0 5px'
    }
    if (direction === 'outbound') {
        switch (callType) {
            case 'missed':
                return <CallMissedOutgoingIcon style={generalStyles} />;
            case 'answered':
                return <CallMadeIcon style={generalStyles} />;
            case 'voicemail':
                return <VoicemailIcon style={generalStyles} />;
            default:
                return <></>;
        }
    }
    if (direction === 'inbound') {
        switch (callType) {
            case 'missed':
                return <CallMissedIcon style={generalStyles} />;
            case 'answered':
                return <CallReceivedIcon style={generalStyles} />;
            case 'voicemail':
                return <VoicemailIcon style={generalStyles} />;
            default:
                return <></>;
        }
    }
    return <></>;
};

export const showDuration = (milliSec: number): string => {
    const oneSec = 1000;
    const limits = {
        minutes: 60 * oneSec,
        hours: 3600 * oneSec,
    }
    if(milliSec > limits.minutes) {
        return `${moment.duration(milliSec, 'milliseconds').minutes()} minutes`;
    }
    if(milliSec > limits.hours) {
        return `${moment.duration(milliSec, 'milliseconds').hours()} hours`;;
    } 
    return `${moment.duration(milliSec, 'milliseconds').seconds()} seconds`;
}

export const getDateInFormat = (date: string) => (
        moment(date).format('MM/DD/YYYY, H:mm')
)