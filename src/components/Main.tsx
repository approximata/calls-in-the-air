import { useState } from 'react';
import { Login } from './Login';
import  { Header }  from './Header';

import { CallList } from './CallList';
import { AuthResponse } from '../interfaces';

const Main = () => {

    const [access, setAccess] = useState<AuthResponse>({
        user: { id: '', username: '' },
        access_token: '',
    });

    return (
        <>
            <Header access={access} setAccess={setAccess} />
            {access.access_token === '' ? (
                <Login access={access} setAccess={setAccess} />
            ) : (
                <CallList access={access} />
            )}
        </>
    );
};

export default Main;
