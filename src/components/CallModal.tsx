import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Call } from '../interfaces';
import { CallCard } from './CallCard';
import { ReactElement } from 'react';

interface Props {
    open: boolean;
    handleClose: () => void;
    call: Call;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        modal: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
    })
);

export const CallModal = (props: Props): ReactElement => {
    const classes = useStyles();
    const { open, handleClose, call } = props;
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby='simple-modal-title'
            aria-describedby='simple-modal-description'
            className={classes.modal}>
            <CallCard call={call!} closeModal={handleClose!} />
        </Modal>
    );
};
