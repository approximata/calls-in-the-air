import { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';

import { LoginProps, UserInput } from '../interfaces';
import useApiAuth from '../queries/use-api-auth';
import { ApiStates } from '../constant';
import { useEffect } from 'react';

interface ValidationModel {
    allValid: boolean;
    helperTextUser: string;
    helperTextPassword: string;
}

const initState = {
    validation: {
        allValid: true,
        helperTextUser: '',
        helperTextPassword: '',
    },
    userInput: {
        username: '',
        password: '',
    },
};

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    error: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

export const Login = ({ setAccess }: LoginProps) => {
    const classes = useStyles();

    const [input, setInput] = useState<UserInput>({ ...initState.userInput });

    const [userInput, setUserInput] = useState<UserInput>({
        ...initState.userInput,
    });

    const [validationResult, setValidationResult] = useState<ValidationModel>(
        initState.validation
    );

    const { user, access_token, apiState, apiError } = useApiAuth(userInput);

    useEffect(() => {
          if (apiState === ApiStates.SUCCESS) {
              setAccess({
                  user,
                  access_token,
              });
          }
    }, [apiState, user, access_token, setAccess])


    const isInputValid = (userInput: UserInput): boolean => {
        const { username, password } = userInput;
        const allValid = username.length !== 0 && password.length !== 0;
        return allValid;
    };

    const clearValidation = () => {
        setValidationResult(initState.validation);
    };

    const handleSubmit = () => {
        if (isInputValid(input)) {
            setUserInput(input);
        } else {
            setValidationResult({
                helperTextUser:
                    input.username.length === 0 ? 'username is missing' : '',
                helperTextPassword:
                    input.password.length === 0 ? 'password is missing' : '',
                allValid: false,
            });
        }
    };

    return (
        <Container component='main' maxWidth='xs'>
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component='h1' variant='h5'>
                    Login
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        variant='outlined'
                        margin='normal'
                        required
                        fullWidth
                        disabled={apiState === ApiStates.LOADING}
                        id='email'
                        label='Email Address'
                        name='email'
                        autoComplete='email'
                        autoFocus
                        helperText={validationResult.helperTextUser}
                        onChange={(e) => {
                            setInput({
                                ...input,
                                username: e.currentTarget.value,
                            });
                            clearValidation();
                        }}
                    />
                    <TextField
                        variant='outlined'
                        margin='normal'
                        required
                        fullWidth
                        disabled={apiState === ApiStates.LOADING}
                        name='password'
                        label='Password'
                        type='password'
                        id='password'
                        autoComplete='current-password'
                        helperText={validationResult.helperTextPassword}
                        onChange={(e) => {
                            setInput({
                                ...input,
                                password: e.currentTarget.value,
                            });
                            clearValidation();
                        }}
                    />
                    <Button
                        type='button'
                        fullWidth
                        variant='contained'
                        color='primary'
                        disabled={apiState === ApiStates.LOADING}
                        className={classes.submit}
                        onClick={handleSubmit}>
                        Login
                    </Button>
                </form>
            </div>
            {apiState === ApiStates.ERROR && (
                <div className={classes.error}>
                    <Alert variant='filled' severity='error'>
                        Something went wrong: {apiError}
                    </Alert>
                </div>
            )}
        </Container>
    );
};
