import { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';
import ExitToAppRoundedIcon from '@material-ui/icons/ExitToAppRounded';
import { LoginProps } from '../interfaces';
import { Tooltip } from '@material-ui/core';
import { useEffect } from 'react';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    })
);

export const Header = ({access, setAccess}: LoginProps) => {
    const classes = useStyles();

    const [isLoggedIn, setIsLoggedIn] = useState(false);
    //const [hasToRefresh, setHasToRefresh] = useState(false)


    useEffect(() => {
        if (access.user.username) {
            setIsLoggedIn(true);
        }
    }, [access.user])

    //useApiRefresh({ hasToRefresh, access, setAccess });
    
    const handleSignOut = () => {
        setAccess({
            access_token: '',
            user: { id: '', username: '' },
        });
        setIsLoggedIn(false)
    };

    return (
        <div className={classes.root}>
            <AppBar position='static'>
                <Toolbar>
                    <Typography variant='h6' className={classes.title}>
                        Call List
                    </Typography>
                    {isLoggedIn && (
                        <>
                            <Tooltip title={access.user.username}>
                                <AccountCircleRoundedIcon />
                            </Tooltip>
                            <Tooltip title='sign out'>
                                <ExitToAppRoundedIcon onClick={handleSignOut} />
                            </Tooltip>
                        </>
                    )}
                </Toolbar>
            </AppBar>
        </div>
    );
}
