import { useState } from 'react';
import {
    DataGrid,
    GridPageChangeParams,
} from '@material-ui/data-grid';

import { AuthResponse, Call } from '../interfaces';
import { ApiStates, PageSize } from '../constant';
import useApiCalls from '../queries/use-api-calls';
import {CallModal} from './CallModal';
import { useCurrentWidth } from '../hooks/use-current-width';
import { useColumns } from '../hooks/use-columns';
import { makeStyles, Theme, createStyles } from '@material-ui/core';

interface CallListProps {
    access: AuthResponse;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        error: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
    })
);

export const CallList = ({ access }: CallListProps) => {
    const classes = useStyles();

    const [page, setPage] = useState<number>(0);
    const { nodes, apiState, apiError, totalCount } = useApiCalls(
        page,
        access.access_token
    );
    const handlePageChange = (params: GridPageChangeParams) => {
        setPage(params.page);
    };

    const [open, setOpen] = useState<boolean>(false);
    const [call, setCall] = useState<Call>();
    const handleOpen = (id: string) => {
        setCall(nodes.find((call) => call.id === id));
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const width = useCurrentWidth();
    const columns = useColumns(width);

    if (apiState === ApiStates.ERROR) {
        return (
            <div className={classes.error}>
                <div>Something went wrong. :( </div>
                <pre>{apiError}</pre>
            </div>
        );
    }
    return (
        <div style={{ height: 700, width: '100%' }}>
            <DataGrid
                rows={nodes}
                columns={columns!}
                pageSize={PageSize}
                rowCount={totalCount}
                paginationMode='server'
                onRowClick={(r) => handleOpen(r.row.id)}
                onPageChange={handlePageChange}
                loading={apiState === ApiStates.LOADING}
            />

            <CallModal open={open} handleClose={handleClose} call={call!} />
        </div>
    );
};

