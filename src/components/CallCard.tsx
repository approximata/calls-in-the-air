import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import NoteIcon from '@material-ui/icons/Note';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import { Call } from '../interfaces';
import { getDateInFormat, mapCall, showDuration } from '../helpers/callHelper';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            maxWidth: 400,
        },
        title: {
            fontSize: 18,
            display: 'flex',
        },
        subTitle: {
            fontSize: 15,
        },
        pos: {
            display: 'flex',
            justifyContent: 'center',
            margin: 12,
        },
        notes: {
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexWrap: 'wrap',
            margin: '5 0',
        },
    })
);
   

interface Props {
    call: Call;
    closeModal: () => void;
}

export const CallCard = (props: Props) => {
    const classes = useStyles();
    const { call, closeModal } = props;
    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography
                    className={classes.title}
                    color='textSecondary'
                    variant='h3'
                    gutterBottom>
                    {mapCall(call.call_type, call.direction)}
                    {call.call_type.toLocaleUpperCase()} CALL
                </Typography>
                <Typography
                    color='textSecondary'
                    className={classes.subTitle}
                    variant='h4'>
                    {call.direction === 'inbound' ? 'incoming' : 'outgoing'} |{' '}
                    {getDateInFormat(call.created_at)} |{' '}
                    {showDuration(call.duration)}
                </Typography>
                <Divider />
                <Typography
                    className={classes.pos}
                    color='textSecondary'
                    variant='body1'>
                    {call.from} ➡ {call.to}
                </Typography>
                <Divider />

                <Typography
                    color='textSecondary'
                    variant='caption'>
                    {call.notes.map((note) => (
                        <p key={note.id} className={classes.notes}>
                            <NoteIcon />
                            {note.content}
                        </p>
                    ))}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size='small' onClick={closeModal}>
                    Close
                </Button>
            </CardActions>
        </Card>
    );
};
